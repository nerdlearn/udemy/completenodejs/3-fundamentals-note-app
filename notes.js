const fs = require('fs');
console.log('Starting notes.js');

var fetchNotes = () => {
	try{
		var notesBuffer = fs.readFileSync('notes-data.json');
		return JSON.parse(notesBuffer);
	} catch(e){
		//if file notes-data.json does not exist this will skip the error and proceed to create file.
		return [];
	}
};

var saveNotes = (notes) => {
	fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
	console.log('addNote', title, body);
	var notes = fetchNotes();
	var note = {
		title,
		body
	};
	
	var duplicateNotes = notes.filter((note) => note.title === title);
	
	if (duplicateNotes.length === 0){
		notes.push(note);
		saveNotes(notes);
		return note;
	}
};

var readNote = (title) => {
	var notes = fetchNotes();
	var foundNoteArray = notes.filter((note) => note.title === title);
	var foundNote = foundNoteArray[0];
    return foundNote;
};

var removeNote = (title) => {
	var notes = fetchNotes();
	var cleanedNotes = notes.filter((note) => note.title !== title);
    saveNotes(cleanedNotes);
	return notes.length !== cleanedNotes.length;
};

var getAll = () => {
	return fetchNotes();
};

var logNote = (note) => {
	debugger;
	console.log('---');
	console.log(`Title: ${note.title}`);
	console.log(`Body: ${note.body}`);
};

var addition = (num1, num2) => {
	console.log('addition');
	console.log('num1:', num1);
	console.log('num2:', num2);
	
	return num1 + num2;
};

module.exports = {
	addNote, 
	logNote,
	getAll, 
	readNote,
	removeNote,
	addition 
};