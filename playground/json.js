const fs = require('fs');

var obj = {
	name: 'Pedro'
};

var stringObj = JSON.stringify(obj);

console.log(typeof stringObj);
console.log(stringObj);

var personString = '{"name":"Pedro","age":"40"}';
var person = JSON.parse(personString);

console.log(typeof person);
console.log(person);

var originalNote = {
	title: 'Some title',
	body: 'Some body'
};

var originalNoteString = JSON.stringify(originalNote);

console.log('typeof originalNoteString:', typeof originalNoteString);
console.log('originalNoteString:',originalNoteString);

fs.writeFileSync('notes.json', originalNoteString);
console.log('file Written');
	
var noteBuffer = fs.readFileSync('notes.json');

console.log('typeof noteBuffer: ', typeof noteBuffer);
console.log('noteBuffer: ',noteBuffer);

var note = JSON.parse(noteBuffer);

console.log('typeof note: ', typeof note);
console.log('note: ',note);

